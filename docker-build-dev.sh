#!/bin/bash

docker build --file 'Dockerfile.dev' $([ "$1" == "no-cache" ] && echo "--$1" || echo "") --tag "bitninja/php-skeleton-dev" .

