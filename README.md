# PHP SKELETON #

This repository contains a general skeleton for PHP development.

Starting point. You have to specialize it for your own needs.



### Directory structure ###

```
.
├── app
│   ├── vendor
├── build
│   ├── coverage
│   ├── logs
│   ├── metrics
│   ├── output
│   ├── pdepend
├── docker
├── jobs
│   ├── builder.groovy
├── tests
│   ├── bootstrap.php
.dockerignore
.gitignore
.php_cs
.phplint.yml
build.xml
composer.json
composer.lock
docker-build.sh
Dockerfile
Jenkinsfile
LICENSE
phpcs.xml
phpmd.xml
phpunit.xml.dist
codeception.yml
README.md
```

| File/folder                | Description                                                                     |
| -------------------------- | ------------------------------------------------------------------------------- |
| app                        | Application source code (in case of documentation project, rename it to docs)   |
| app/vendor                 | 3rd party libraries for the source code                                         |
| build                      | Working directory for build and test process                                    |
| build/coverage             | Code coverage in HTML format                                                    |
| build/logs                 | Metrics in XML format                                                           |
| build/metrics              | PHPMetrics in HTML format                                                       |
| build/output               | Console output of build steps                                                   |
| build/pdepend              | PDepend charts                                                                  |
| docker                     | Docker image fixtures. Configuration, services, etc                             |
| jobs                       | Jenkins build jobs                                                              |
| *tests*                    | *Use it, if framework doesn't have built-in test module (optional)*             |
| .dockerignore              | Paths for Docker not to copying into the image                                  |
| .gitignore                 | Paths for Git not to commit                                                     |
| .php_cs                    | PHP Coding Standard Fixer configuration                                         |
| .phplint.yml               | PHPLint configuration                                                           |
| *.scrutinizer.yml*         | *Scrutinizer CI configuration (optional)*                                       |
| *.travis.yml*              | *Travis CI configuration (optional)*                                            |
| *bitbucket-pipelines.yml*  | *BitBucket build pipelines (optional)*                                          |
| build.xml                  | Phing build file with build targets                                             |
| composer.json              | Dependency management                                                           |
| composer.lock              | Dependency snapshot                                                             |
| docker-build.sh            | Docker image build script for local testing images before sending to registry   |
| *docker-build-dev.sh*      | *Docker image build script for images with dev-specific settings (optional)*    |
| *versioning.sh*            | *Useful if your Pipeline contains automatic Git tagging (optional)*             |
| Dockerfile                 | Docker image definition                                                         |
| *Dockerfile.dev*           | *Docker image definition for development environment (optional)*                |
| Jenkinsfile                | Jenkins build pipeline                                                          |
| *LICENSE*                  | *License information (optional)*                                                |
| phpcs.xml                  | PHP CodeSniffer configuration for coding conventions                            |
| *phpdox.xml*               | *PHPDox document generator configuration (optional)*                            |
| phpmd.xml                  | PHP Mess Detector configration                                                  |
| phpunit.xml[.dist]         | PHPUnit configuration (not necessary if Codeception is configured)              |
| codeception.yml            | Codeception configuration file                                                  |
| README.md                  | Project description; contains config and deployment how-to                      |



### Future plans ###

Make public and create a packagist package. Then we are able to create new project with Composer easily

```
composer create-project --prefer-dist --stability=dev bitninja/php-skeleton new-project
```