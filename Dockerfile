#
#  From this base-image / starting-point
#
FROM ...

#
#  Authorship
#
LABEL maintainer "Joseph Palfi <joseph@bitninja.io>"

#
#  Information about container image
#  See: http://label-schema.org/rc1/
#
LABEL org.label-schema.build-date="2016-12-25T16:00:00.00Z" \
      org.label-schema.name="php-skeleton" \
      org.label-schema.description="PHP application skeleton" \
      org.label-schema.usage="" \
      org.label-schema.url="https://bitbucket.org/bitninjaio/php-skeleton" \
      org.label-schema.vcs-url="git@bitbucket.org:bitninjaio/php-skeleton.git" \
      org.label-schema.vcs-ref="" \
      org.label-schema.vendor="BitNinja Kft" \
      org.label-schema.version="1.0.1" \
      org.label-schema.schema-version="1.0.0-rc.1" \
      org.label-schema.docker.cmd="docker run -d -v /.../...:/.../... bitninja/php-skeleton" \
      org.label-schema.docker.cmd.devel="docker run -d -v /.../...:/.../... -v /vagrant:/vagrant bitninja/php-skeleton" \
      org.label-schema.docker.cmd.test="" \
      org.label-schema.docker.debug="" \
      org.label-schema.docker.cmd.help="" \
      org.label-schema.docker.params=""

#
#  Install programs into image
#
RUN ...

#
#  Predefined settings
#
COPY docker/ /

#
#  Copying files into the image
#
ENV APP_DIR=/home/app
#WORKDIR $APP_DIR

COPY app/ $APP_DIR

#
#  Update dependencies
#
#RUN composer update --no-dev

#
#  Set up owner and access rights
#
RUN    chown -R webuser.webuser $APP_DIR \
#    && chmod 600 /opt/... /etc/... \
    && find $APP_DIR/* -type d -print0 | xargs -0 chmod 0755 \
    && find $APP_DIR/* -type f -print0 | xargs -0 chmod 0644

#
#  Set up what user the container should run as
#  Important: use uid instead of username!
#
USER <uid>
